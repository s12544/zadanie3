package db;

import java.util.List;

import Domain.Person;

public interface Repository<TEntity> {

    public Person withId(int id);
    public List<Person> allOnPage(PagingInfo page);
    public void add(TEntity entity);
    public void modify(TEntity entity);
    public void remove(TEntity entity);
}